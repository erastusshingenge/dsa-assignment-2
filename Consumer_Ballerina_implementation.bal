


import ballerina/log;
import ballerina/sql;
import ballerinax/kafka;


sql:Client sqlClient = check new('C:\Users\Dominique ITP\Downloads\JSONfile.json', SINGLE_CLOB);

kafka:ConsumerConfiguration configurationsForConsumers = {
    groupId: "group-id",
    topics: ["storage-system"]
};

string endPoints = "localhost:9092";

listener kafka:Listener theListener = new (endPoints, configurationsForConsumers);

function kafkaCheckerProcessor(kafka:ConsumerRecord kafkaRecord) returns error? {

    //message in byte 
    byte[] byteMessage = kafkaRecord.value;

    //message in string mode   
    string stringMessage = check string:fromBytes(byteMessage);
    log:printInfo("The following message was retrieved: " + stringMessage);
}

//Kafka service that listens from the topic 'storage-system'
service kafka:Service on theListener {

    remote function onConsumerRecord(
        kafka:Caller caller,kafka:ConsumerRecord[] records) returns error? {

        foreach var kafkaRecord in records {
            check kafkaCheckerProcessor(kafkaRecord);
        }

    }
}


