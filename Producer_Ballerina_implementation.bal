import ballerina/uuid;
import ballerina/io;
import ballerina/sql;
import ballerinax/kafka;


sql:Client sqlClient = check new('C:\Users\Dominique ITP\Downloads\JSONfile.json', SINGLE_CLOB);


//Links client ID, MAximum amount of attempts permitted and a maximum size of

kafka:ProducerConfiguration configurationsForProducers = {
    clientId: "storage-system-producer",
    retryCount: 3,
    sendBuffer: 30
};



string endPoints = "localhost:9092";
kafka:Producer producerForKafka = check new (endPoints, configurationsForProducers);

public function main() {
    json message = {
        output: "Ballerina services are successful"
    };
.,
    byte[] finalOutput = message.toBalString().toBytes();

    kafkaTransaction(finalOutput);

}
    //The transaction is made into a function with four features
function kafkaTransaction(byte[] output) {
    transaction {
        kafka:Error? failure = producerForKafka->send({
            topic: "storage-system",
            value: output,
            key: uuid:createType1AsString().toBytes()
        });

        //Error checker
        if failure is kafka:Error {
            io:println("Please fix the following error:", failure);
        }
        //confirms id the transaction was successful or not
        var  transactionStatus = commit;
        if transactionStatus is () {
            io:println("Congradulations on a successful transaction!");
        } else {
            io:println("I'm sorry, your transaction didn't go through. Here is why: " + transactionStatus.message());
        }
    }
}
