SELECT @JSON = userinformation

FROM OPENROWSET
 (BULK 'C:\Users\Dominique ITP\Downloads\JSONfile.json', SINGLE_CLOB)
AS e 

SELECT ISJSON(@JSON)

If(ISJSON(@JSON)=1)

SELECT @JSON AS 'JSON text'

SELECT studnum, a, c, , boolean, color, Names
INTO DSAassignment
  FROM JSONfile(@JSON)
  WITH (id Integer(9),
    a char,
    c char,
    studnum INT,
    Names VARCHAR(25),
    color VARCHAR(5),